# Geezerpin frontend-dev challenages

## Properties List - Small web app challenge

In this challenge, you need to create a web application that lists properties for an online property rental website. The web application would be embedded in the public display devices on the streets or on the buses for advertisements.

To show case your frontend skills, you are asked to implement the minimum of the web application. However, it is not required to be exactly the same as what you can see on the wireframe.

**The framework that you can use to create the website is limited to AngularJS or ReactJS.** The use of CSS preprocessors, such as SASS/Prepros.io, is highly recommended.

The web app that you create needs to be responsive. That is, it supports desktops and mobile devices. The wireframe for desktop is provided in the `properties-list` folder, the mobile version is up to your creativity.

In the `properties-list` folder, you can also find the API document. You will fetch the properties data throught the API calls. The API endpoint is `https://api.dwellease.co`.
