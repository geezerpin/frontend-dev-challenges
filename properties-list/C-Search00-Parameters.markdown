Route: `GET` `/parameters/search`

Body:

```
(empty)
```

Response Code: `200`

Response:
```
{
    "streets": ["Orchard", "somerest"],
    "price_ranges": [{low:0, high: 2000}, {low: 2000, high: 4000}, {low: 4000, high: 6000}],
    "num_bedrooms": ["1", "2", "3", "4", "5", "5+"],
    "num_bathrooms": ["1", "2", "3", "4", "5", "5+"],
    "types": ["Condominium", "House", "HDB"],
    "furnishing": ["Not furnished", "Partially furnished", "Fully furnished"],
    "facilities": ["Pool", "Maids room", "Barbecue Area", "Sea view"],
    "transport": ["Cloes to MRT", "Cloes to bus stop"],
    "popular_keywords": ['orchard', 'somerest']
}
```

