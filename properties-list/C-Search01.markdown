Route: `POST` `/search`

Body:

```
{
    "keyword": "Balcony",
    "sort_by": "price_asc", // possible values: price_desc, price_asc, size_desc, size_asc
    "result_size": 4, // page_size
    "skip": 4, // page_num * page_size. eg. 0 when page 1, 1*page_size when page 2, etc.

    "location": "Orchard",
    "price_low": 500,
    "price_high": 1000,
    "num_bedrooms": ["2"], // possible values: "1", "2", "3", "4", "5", "5+"
    "num_bathrooms": ["1"], // possible values: "1", "2", "3", "4", "5", "5+"
    "size_min": 0,
    "size_max": 0,
    "type": "condominium",
    "furnishing": "Fully furnished",
    "year_built_min": 2000,
    "year_built_max": 2010,
    "facilities": ["Pool", "Maids room"],
    "transport": "Close to MRT"
}
```

Response Code: `200`

```
{
    "num_matched": 100,
    "properties": [{
        "property_id": "e0b4d672-a090-4f66-87f5-a929ea65ef11",
        "listing_date": "2017-06-10T04:53:27+00:00",
        "landlord": {
            "name": "Henry Ong",
            "profile_image": "https://somewhere.com/path/to/avatar.png"
        },
        "photo-large": "https://somewhere.com/path/to/large-photo.png",
        "photo-small": "https://somewhere.com/path/to/small-photo.png",
        "other_photos": [
            "https://somewhere.com/path/to/other-photo1.png",
            "https://somewhere.com/path/to/other-photo2.png"
        ],
        "landlord_thoughts": "Something to say...",
        "name": "Rosehill District",
        "type": "condominium",
        "street": "Somerest",
        "monthly_rental": 4200,
        "num_bedrooms": 5,
        "num_bathrooms": 3,
        "size_sqft": 821,
        "lease_term": "6 months",
        "lease_terms": ["6 months", "12 months"],
        "furnishing": "Fully furnished",
        "closest_mrt": "Orchard MRT station, 3KM",
        "amenities": "Pool, BBQ Area",
        "year_built": 2005,
        "special": "Maids room, Sea view, Quite place",
        "status": "approved",
        "wishlisted": false
    }]
}
```

